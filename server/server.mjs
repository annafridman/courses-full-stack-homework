
import express from "express";
import cors from 'cors';
import mysql from 'mysql';

const app = express();
app.use(express.json()); //Used to parse JSON bodies
app.use(express.urlencoded()); //Parse URL-encoded bodies
app.use(cors());

//MySQL-create connection
//a connection pool is a cache of database connections maintained so that the connections can be reused when future requests to the database are required
const pool = mysql.createPool({
    connectionLimit : 10,//By default, SQL Server allows a maximum of 32767 concurrent connections which is the maximum number of users that can simultaneously log in to the SQL server instance.
    user: 'uuxcegdmba9et',
    host:'c58793.sgvps.net',
    password: 'cbgf66yt2plw',
    database: 'dbu285dkqs78on'
})

//Connect
pool.getConnection(function(err) {
    if(err) throw err;
    console.log('MySql Connected...')
})

//Get courses
app.get('/api/courses', (req, res)=>{
    pool.query('SELECT * FROM track_name', (err, rows) => {
        if(err) throw err;
        res.send(rows)
    })
})


//Create table
app.get('/createarticipantstable', (req, res) => {
    let sql = 'CREATE TABLE participants( fullName VARCHAR(30) NOT NULL, email VARCHAR(50) NOT NULL, phone CHAR(15), chosenCourses TEXT NOT NULL)';
    pool.query(sql, (err, result) => {
        if(err)  throw err;
        res.send('participant table created');
    })
})

//Post course participant
app.post('/api/participants', (req, res) => {
    let fullName = req.body.fullName;
    let email = req.body.email;
    let phone = req.body.phone;
    let chosenCourses = req.body.chosenCourses;
 
    pool.query(`INSERT INTO participants (fullName, phone, email, chosenCourses) VALUES (?,?,?,?)`,
     [fullName, phone, email, chosenCourses],
     (err)=>{
         if (err) throw err;
         res.send('participant added');
     }    
 );
})

const port = process.env.PORT || 3306; 
app.listen(port, () => console.log(`Listening on port ${port}`));
