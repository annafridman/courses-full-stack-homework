import { useContext, useEffect, useState } from 'react';
import './Course.css';
import {CoursesContext} from './CoursesContext.js';


export default function Course({
    id,
    track_name,
    is_open,

}) {
    
    const {addCourse} = useContext(CoursesContext);
    const [canChoose, setCanchoose] = useState(true);

    useEffect(() =>{
        if(is_open === "n"){
            setCanchoose(false);
        }
    }, [is_open]);

    return (
        <div className="course">
            {track_name}
            <span>Click for more info...</span>
            {canChoose ? <button onClick={() => addCourse(track_name)}><span>Add course</span></button> : <span> Course temporarily unavailable</span>}
        </div>
    );
}

