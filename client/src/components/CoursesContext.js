import React, {useCallback,useState} from 'react';



export const CoursesContext = React.createContext({
    courses: [],
    setCourses: () => [],
    getCourses: ()=> [],
    chosenCourses: [],
    setChosenCourses: ()=>[],
    addCourse: ()=> []
});


export default function CoursesProvider({children}) {
const [courses, setCourses] = useState([]);
const [chosenCourses, setChosenCourses] = useState([]);


const getCourses = useCallback(()=> {
    
    fetch('/api/courses')
    .then(response =>response.json())
    .then(coursesArr => setCourses(coursesArr))
    
  }, []);

  const addCourse = track_name => {
    setChosenCourses([...chosenCourses, track_name])
    console.log(`${track_name} added`)
  }

  

    return (
        <CoursesContext.Provider value={{
            courses,
            setCourses,
            getCourses,
            addCourse,
            chosenCourses,
            setChosenCourses
        }}>
            {children}    
        </CoursesContext.Provider>
       
    );
    }    
    