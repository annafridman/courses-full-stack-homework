import './Courses.css';
import React, {useContext} from 'react';
import {CoursesContext} from './CoursesContext.js';
import Course from './Course.js';

function Courses() {
 const {courses} = useContext(CoursesContext);

  return (
    <div className="courses">
     
              {courses.length > 0 
              ? courses.map(course => <Course key={course.track_name} {...course} />)                                                                                                                 
              :"LOADING"}

    </div>
  );
}

export default Courses;
