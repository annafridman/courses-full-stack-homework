import './Form.css';
import {useContext, useState} from 'react';
import {CoursesContext} from './CoursesContext.js';



export default function FormWithValidations() {
const [fullName, setFullName] = useState('');
const [phone, setPhone] = useState('');
const [email, setEmail] = useState('');
let data = {
  fullName: fullName,
  phone:phone,
  email: email
}

const {chosenCourses, setChosenCourses} = useContext(CoursesContext);

const handleChange = (e) => {
  switch (e.target.id) {

      case "fullName":
        setFullName(e.target.value);
        break;

      case "phone":
        setPhone(e.target.value);           
        break;

       case "email":
         setEmail(e.target.value);
         break;

         default: return "enter valid value";
  }
};

const clear = ()=>{
  setFullName("");
  setPhone("");
  setEmail("");
  setChosenCourses([]);

}


const handleSubmit = (e) => {
  e.preventDefault();

  if (email !== '' &&  fullName !== '' &&  chosenCourses.length > 0){
    alert('Form submited');
  }else{
    alert('Fill all form inputs');
  }



  data={...data, chosenCourses: chosenCourses.join() };


  fetch('/api/participants',
    {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)

    })

    clear();
};

  return (
    <>

    <form>
      <h1>Join us!!!</h1>
      <div>
      <input
      required
      id="fullName"
      type="text"
      placeholder="Full Name"
      value={fullName}
      onChange={handleChange} />
      </div>

      <div>
      <input
      id="phone"
      type="number"
      placeholder="Phone"
      value={phone}
      onChange={handleChange} />
      </div>

      <div>
        <input
        required
      id="email"
      type="email"
      placeholder="Email"
      value={email}
      onChange={handleChange} />
       </div>

       <div>
        <input
        required
      id="courses"
      type="text"
      placeholder="Chosen courses"
      value={chosenCourses}
      onChange={handleChange} />
       </div>

      <button onClick={handleSubmit}>SUBMIT</button>

    </form>
    </>
  )
}