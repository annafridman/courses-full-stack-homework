
import './App.css';
import React, {useContext, useEffect} from "react";
import Courses from './Courses.js';
import Form from './Form.js';
import {CoursesContext} from './CoursesContext.js';

function App() {

  const {getCourses} = useContext(CoursesContext);

  useEffect(() => {
    getCourses()
  }, [])

  return (
    <div className='App'>
    <div className="wrapper">
      <h1>Check out our courses:</h1>
      <Courses />
      <Form />
    </div>
    </div>
  );
}

export default App;
